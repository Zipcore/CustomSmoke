#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <emitsoundany>

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = 
{
	name = "Custom Smoke",
	author = "Zipcore",
	description = "",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net"
}

public void OnPluginStart()
{
	CreateConVar("custom_smoke_version", PLUGIN_VERSION, "", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	
	AddNormalSoundHook(NormalSHook);
	HookEvent("smokegrenade_detonate", Event_OnSmokegrenadeDetonatePre, EventHookMode_Pre);
}

public Action NormalSHook(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags)
{
	if(StrContains(sample, "smoke_emit.wav") != -1)
		return Plugin_Stop;
	
	return Plugin_Continue;
}

public Action Event_OnSmokegrenadeDetonatePre(Handle hEvent, const char[] sName, bool bDontBroadcast)
{
	int iEntity = GetEventInt(hEvent, "entityid");
	
	RemoveEdict(iEntity);
	
	float fPos[3];
	fPos[0] = float(GetEventInt(hEvent, "x"));
	fPos[1] = float(GetEventInt(hEvent, "y"));
	fPos[2] = float(GetEventInt(hEvent, "z"));
	
	CreateCustomSmoke(fPos);
	
	return Plugin_Stop;
}

void CreateCustomSmoke(float fPos[3])
{
	float fFadestart = 1.0;
	float fFadeend = 15.0;
	
	int iEntity = CreateEntityByName("env_particlesmokegrenade");

	if (iEntity != -1)
	{
		SetEntProp(iEntity, Prop_Send, "m_CurrentStage", 1);
		SetEntPropFloat(iEntity, Prop_Send, "m_FadeStartTime", fFadestart);
		SetEntPropFloat(iEntity, Prop_Send, "m_FadeEndTime", fFadeend);
		
		DispatchSpawn(iEntity);
		ActivateEntity(iEntity);
		
		TeleportEntity(iEntity, fPos, NULL_VECTOR, NULL_VECTOR);
		
		RemoveEntity(iEntity, fFadeend);
		
		PrecacheSoundAny("weapons/smokegrenade/sg_explode.wav");
		EmitSoundToAllAny(.sample = "weapons/smokegrenade/sg_explode.wav", .entity = iEntity, .speakerentity = iEntity);
	}
	
	fPos[2] += 32.0;
	
	int iEntity2 = CreateEntityByName("env_particlesmokegrenade");

	if (iEntity2 != -1)
	{
		SetEntProp(iEntity2, Prop_Send, "m_CurrentStage", 1);
		SetEntPropFloat(iEntity2, Prop_Send, "m_FadeStartTime", fFadestart);
		SetEntPropFloat(iEntity2, Prop_Send, "m_FadeEndTime", fFadeend);
		
		DispatchSpawn(iEntity2);
		ActivateEntity(iEntity2);
		
		TeleportEntity(iEntity2, fPos, NULL_VECTOR, NULL_VECTOR);
		
		RemoveEntity(iEntity2, fFadeend);
	}
}

stock void RemoveEntity(entity, float time = 0.0)
{
	if (time == 0.0)
	{
		if (IsValidEntity(entity))
		{
			char edictname[32];
			GetEdictClassname(entity, edictname, 32);

			if (!StrEqual(edictname, "player"))
				AcceptEntityInput(entity, "kill");
		}
	}
	else if(time > 0.0)
		CreateTimer(time, RemoveEntityTimer, EntIndexToEntRef(entity), TIMER_FLAG_NO_MAPCHANGE);
}

public Action RemoveEntityTimer(Handle Timer, any entityRef)
{
	int entity = EntRefToEntIndex(entityRef);
	if (entity != INVALID_ENT_REFERENCE)
		RemoveEntity(entity);
	
	return (Plugin_Stop);
}